\babel@toc {french}{}
\contentsline {section}{\numberline {1}Rappel}{2}{section.1}%
\contentsline {section}{\numberline {2}Interpolation Polynomiale}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Interpolation de Neville}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Interpolation de Newton}{2}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Comparaison des deux methodes d'interpolation}{3}{subsection.2.3}%
\contentsline {section}{\numberline {3}Approximation}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1} Regression linéaire}{3}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2} Regression linéaire: ajustement exponentiel}{4}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3} Regression linéaire: ajustement puissance}{5}{subsection.3.3}%
