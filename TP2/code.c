#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>


typedef struct donnee{
	double * x;
	double * y;
	int n;
}donnee;

typedef struct tableau{
	double * data;
	int n;
}tableau;

int allocate_memory_tableau(tableau * tab,int n){
	//fonction allouant des espaces dynamiques à des tableau (tableau à une dimension)
	tab->n = n;
	tab->data= malloc(n*sizeof(double*));
	/*test pour vérifier si l'allocation a marché*/
	if(tab->data==NULL){return -1;}
	return 0;
}

int allocate_memory_data(donnee * data,int n){
	//fonction allouant des espaces dynamiques à des tableau (tableau à une dimension)
	data->n = n;
	data->x= malloc(n*sizeof(double*));
	/*test pour vérifier si l'allocation a marché*/
	if(data->x==NULL){return -1;}
	/*test pour vérifier si l'allocation a marché*/
	data->y= malloc(n*sizeof(double*));
	if(data->y==NULL){return -1;}
	return 0;
}

/*cette partie contient les fonction en charge de l'affichage */
void affiche_tableau(tableau * tab, int n){
//fonction affichant les valeur de la matrice  passé en paramètre
	int i;
	for(i=0;i<n;i++){
			printf("%lf ",tab->data[i]);
	}
	printf("\n");
}

//interpolation polynomiale: méthodes de Newton et Neville 
/*Méthode de Neville*/
double neville(donnee* data,double x){
	int n=data->n;
	tableau * resultat=malloc(sizeof(tableau));
	allocate_memory_tableau(resultat,n);
	//etape initiale: P0[Xi]
	int i,k;
	for(i=0;i<n;i++){
		resultat->data[i]=data->y[i];
		//affiche_tableau(resultat,3);
	}
	//evalution Pn-1(x)
	for (k = 1; k < n; k++) {
		for (i = 0; i < n - k; i++) {
            		resultat->data[i] = (x - data->x[i+k]) * resultat->data[i];
            		resultat->data[i] -= (x - data->x[i]) * resultat->data[i + 1];
           		resultat->data[i] /= (data->x[i] - data->x[i+k]);
        	}
        	//printf("etape %d: %lf\n",k,resultat->data[0]);
    	}
    	double res=resultat->data[0];
    	free(resultat->data);
	return res;
}
/*Méthode de Newton*/

double newton(donnee* data, double x){
	int n=data->n;
	double b[n];
	//initialisation de b avec les valeur de y du tableau de donnée
	for (int i=0; i<n; i++){
		b[i]= data->y[i];
	}
	//calcul des différences divisées 
	for (int i=0; i<n; i++){
	    for (int j=n-1; j>i; j--){
	    	b[j]=(b[j] - b[j-1]) / (data->x[j] - data->x[j-i-1]);
	    }
	    //printf("etape %d: %lf\n",i,b);
	}
	//évalution de P(x)
	double res=b[n-1];
	for (int i=n-1; i>0; i--){
        	res=b[i-1] + res * (x - data->x[i-1]);
        	//printf("etape %d: %lf\n",i,res);
        }
    	return res; 
}

// approximation 
//calcul de la droite de regression y=ax+b
typedef struct droite{
	double a;
	double b;
} droite;

droite droite_de_regression(donnee* data){
	droite coord; // structure contenant a et b de ax+b
	double moy_xy=0,moy_x=0,moy_y=0,moy_x2=0;
	for(int i=0; i<data->n; i++){
		moy_x += data->x[i]; //somme de tous les x pour le calcul de la moyenne
		moy_y += data->y[i]; //somme de tous les y pour le calcul de la moyenne
		moy_x2 += data->x[i] * data->x[i]; //somme de tous les x^2 pour le calcul de la moyenne
		moy_xy += data->x[i] * data->y[i]; //somme de tous les x*y pour le calcul de la moyenne
	}
	/*les moyennes*/
	moy_x /= data->n;
	moy_y /= data->n;
	moy_x2 /= data->n;
	moy_xy /= data->n;
	//calcul de a et b
	coord.a= (moy_xy - moy_x * moy_y) / (moy_x2 - (moy_x * moy_x));
	coord.b= moy_y - (coord.a * moy_x);
	return coord;
}

double eval_droite_regression(donnee* data, double x){
	droite y=droite_de_regression(data);
	return y.a*x+y.b;
}
//ajustement exponentiel
float ajust_expo(donnee* data,double x){
	for(int i=0; i<data->n; i++){
		printf("etape %d\n",i);
		printf("y ava log%lf\n",data->y[i] );
		data->y[i] = log(data->y[i]);
		printf("y apres log%lf\n",data->y[i] );
	}
	droite y=droite_de_regression(data);
	double c=exp(y.a);
	double d=y.b;
	printf ("%lf %lf %lf\n",c,d,x);
	return c*exp(d * x);
}

//ajustement puissance
double ajust_puissance(donnee* data,double x){
	for(int i=0; i<data->n; i++){
		data->x[i] = log(data->x[i]);
		data->y[i] = log(data->y[i]);
	}
	droite y=droite_de_regression(data);
	return exp(y.a)*pow(x,y.b);
}


int main() {
	donnee *data=malloc(sizeof(donnee));
	allocate_memory_data(data,11);
/*	
	//les valeurs de x
	data->x[0]=0;
	data->x[1]=2;
	data->x[2]=4;
	data->x[3]=6;
	data->x[4]=8;
	data->x[5]=10;
	data->x[6]=12;
	data->x[7]=14;
	data->x[8]=16;
	data->x[9]=18;
	data->x[10]=20;
	data->x[11]=22;
	data->x[12]=24;
	data->x[13]=26;
	data->x[14]=28;
	data->x[15]=30;
	data->x[16]=32;
	data->x[17]=34;
	data->x[18]=36;
	data->x[19]=38;
	//les valeurs de y
	data->y[0]=0.99987;
	data->y[1]=0.99997;
	data->y[2]=1.00000;
	data->y[3]=0.99997;
	data->y[4]=0.99988;
	data->y[5]=0.99973;
	data->y[6]=0.99953;
	data->y[7]=0.99927;
	data->y[8]=0.99897;
	data->y[9]=0.99846;
	data->y[10]=0.99805;
	data->y[11]=0.999751;
	data->y[12]=0.99705;
	data->y[13]=0.99650;
	data->y[14]=0.99664;
	data->y[15]=0.99533;
	data->y[16]=0.99472;
	data->y[17]=0.99472;
	data->y[18]=0.99333;
	data->y[19]=0.99326;
	
*/	
	//les valeurs de x
	data->x[0]=10;
	data->x[1]=8;
	data->x[2]=13;
	data->x[3]=9;
	data->x[4]=11;
	data->x[5]=14;
	data->x[6]=6;
	data->x[7]=4;
	data->x[8]=12;
	data->x[9]=7;
	data->x[10]=5;
	
	//les valeurs de y
	data->y[0]=8.04;
	data->y[1]=6.95;
	data->y[2]=7.58;
	data->y[3]=8.81;
	data->y[4]=8.33;
	data->y[5]=9.96;
	data->y[6]=7.24;
	data->y[7]=4.26;
	data->y[8]=10.84;
	data->y[9]=4.82;
	data->y[10]=5.68;
	
/*	
	data->x[0]=88;
	data->x[1]=89;
	data->x[2]=90;
	data->x[3]=91;
	data->x[4]=92;
	data->x[5]=93;
	data->x[6]=94;
	data->x[7]=95;
	data->x[8]=96;
	data->x[9]=97;
	
	
	//les valeurs de y
	data->y[0]=5.89;
	data->y[1]=6.77;
	data->y[2]=7.87;
	data->y[3]=9.11;
	data->y[4]=10.56;
	data->y[5]=12.27;
	data->y[6]=13.92;
	data->y[7]=15.72;
	data->y[8]=17.91;
	data->y[9]=22.13;

	
*/	
	
	//test
	
	/*printf("\ntableau d'essaie sur les points allant de 0 à 15 avec le polynôme généré par la methode neville\n");
	for(int k = 0; k<=20; k++){
		printf("%lf ",(double)(k));
	}
	printf("\n");
	for(int k = 0; k<=90; k++){
		printf("%lf ",neville(data,k));
	}
	printf("\ntableau d'essaie sur les points allant de 0 à 15 avec le polynôme généré par la methode newton\n");
	for(int k = 0; k<=15; k++){
		printf("%lf ",(double)(k));
	}
	printf("\n");
	for(int k = 0; k<=15; k++){
		printf("%lf ",newton(data,k));
	}
	printf("\n");*/
	
	//droite coord=droite_de_regression(data);
	//droite coord2=ajust_puissance(data);
	
	
	
	printf("Jeu d'essai 4.3  droite de regression\n");
	for(int k = 0; k<=15; k++){
		printf("%lf ",(double)(k));
	}
	printf("\n");
	for(int k = 0; k<=15; k++){
		printf("%lf ",eval_droite_regression(data,k));
	}
	
	printf("\n");
	
	
	
	
	/*printf("Jeu d'essai 4.3  droite de regression ajustement exponentiel\n");
	for(int k = 0; k<=15; k++){
		printf("%lf ",(double)(k));
	}
	printf("\n");
	for(int k = 0; k<=15; k++){
		printf("%lf ",ajust_expo(data,k));
	}
	printf("\n");*/
	
	//printf("l'ajustement exponetiel de la droite de regression est %f\n",ajust_expo(data,90));
	
	/*printf("la droite de regression est %lf\n",eval_droite_regression(data,5));
	printf("l'ajustement exponetiel de la droite de regression est %lf\n",ajust_expo(data,5));
	printf("l'ajustement puissance de la droite de regression est %lf\n",ajust_puissance(data,5));

	//affiche_tableau(neville(data,0),3);*/
	return 0;
}
